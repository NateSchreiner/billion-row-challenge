package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"sync"
	"syscall"
	"time"
)

type Undone struct {
	parentOffStart int
	offset         int
	bytes          []byte
}

type Pair struct {
	name string
	val  float64
}

type Station struct {
	runningTotal float64
	runningCount int
	min          float64
	max          float64
}

// TODO:: process a text file of weather station names and temperatures,
// and for each weather station, print out the minimum, mean, and maximum

func playground() {
	f, err := os.Open("small.txt")
	if err != nil {
		panic(err)
	}

	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		panic(err)
	}

	data, err := syscall.Mmap(int(f.Fd()), 0, int(stat.Size()), syscall.PROT_READ, syscall.MAP_SHARED)
	if err != nil {
		panic(err)
	}

	p := func(d []byte) {
		for len(d) > 0 {
			index := bytes.Index(d, []byte{'\n'})
			if index >= 0 {
				tmp := d[:index]
				d = d[index+1:]
				fmt.Println(string(tmp))

			}
			fmt.Println(string(d))
			d = []byte{}
		}
	}

	p(data)

	syscall.Munmap(data)
}

const INPUT_FILE = "measurements.txt"
const TEST_FILE = "test.txt"

var ROUTINES = runtime.NumCPU() * 2

func main() {
	fmt.Printf("CPUs: %d\n", runtime.NumCPU())

	f, err := os.Open(TEST_FILE)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("File size: %dGb\n", stat.Size()/(1024*1024*1024))

	defer func(t time.Time) {
		fmt.Printf("Runtime: %v\n", time.Since(t))
	}(time.Now())

	data, err := syscall.Mmap(int(f.Fd()), 0, int(stat.Size()), syscall.PROT_READ, syscall.MAP_SHARED)
	if err != nil {
		log.Fatal(err)
	}
	dataLen := len(data)
	var wg sync.WaitGroup

	chunkSize := len(data) / ROUTINES
	fmt.Printf("Optimal chunk size: %d\n", chunkSize)
	pairChan := make(chan Pair)
	stationMap := make(map[string]*Station, 0)

	routine := 0

	go func() {
		for {
			select {
			case pair := <-pairChan:
				val, ok := stationMap[pair.name]
				if ok {
					addToStation(val, pair.val)
				} else {
					station := Station{
						runningTotal: pair.val,
						runningCount: 1,
						max:          pair.val,
						min:          pair.val,
					}
					stationMap[pair.name] = &station
				}
			}
		}
	}()

	totalBytes := 0

	process := func(d []byte) {
		totalBytes = totalBytes + len(d)
		defer wg.Done()

		fmt.Printf("Processing chunk-size: %d\n", len(d))

		for len(d) > 0 {
			idx := bytes.Index(d, []byte{'\n'})
			if idx == 0 {
				break
			}
			var tmp []byte
			if idx > 0 {
				tmp = d[:idx]
				d = d[idx+1:]
			} else {
				tmp = d
				d = []byte{}
			}

			s := bytes.Split(tmp, []byte{';'})
			name, val := s[0], s[1]
			v, err := strconv.ParseFloat(string(val), 64)
			if err != nil {
				panic(err)
			}
			station := Pair{
				name: string(name),
				val:  v,
			}

			pairChan <- station
		}
	}

	for routine < ROUTINES {
		buf := data[0:chunkSize]
		index := bytes.LastIndex(buf, []byte{'\n'})
		buf = data[0:index]
		data = data[index:]

		wg.Add(1)
		process(buf)

		routine++
	}

	wg.Wait()
	// Super hacky. Since the chunkSize is runtime dependant on closest '\n'
	// we're going to have one last slice to process
	wg.Add(1)
	process(data)

	syscall.Munmap(data)

	fmt.Printf("Proccessed: %d with data len: %d\n", totalBytes, dataLen)
	fmt.Printf("Found: %d number of stations\n", len(stationMap))
}

func addToStation(station *Station, val float64) {
	station.runningCount++
	station.runningTotal += val
	if station.max < val {
		station.max = val
	} else if station.min > val {
		station.min = val
	}
}
